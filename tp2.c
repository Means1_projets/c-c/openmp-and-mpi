#include <stdio.h>
#include "omp.h"
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

// Size of the matrix
#define WIDTH 50
#define HEIGHT 50

//Number to Find
#define numberToFind 1010


int matrix[HEIGHT][WIDTH];

// Transform a integer to a string
char* itoa(int val, int base){
	static char buf[32] = {0};
	int i = 30;
	for(; val && i ; --i, val /= base)
		buf[i] = "0123456789abcdef"[val % base];
	return &buf[i+1];
}

// Find the min between the two param 
int min(int a, int b){
	if (a > b)
		return b;
	return a;
}

// Find the max between the tow param
int max (int a, int b){
	if (a < b)
		return b;
	return a;
}

// Check if the position i,j is in the blacklist
int isIn(int i, int j, int blacklist[], int size){
	int pos = 0;
	while (pos < size){
		if (i == blacklist[pos] && j == blacklist[pos+1])
			return 1;
		else
			pos = pos + 2;
	}
	return 0;
}

//Copy the matrix
void matriscopy (void * destmat, void * srcmat, int size) 
{
  memcpy(destmat,srcmat, size*sizeof(int));
}

// This function searches recursively for the next number among the neighbors of the position set as argument. 
// Param : 
// i : the pos
// j : the pos
// numToFindTab : number to find
// pos : position of the digit to find
// size : size of the numberToFind
// blacklist : The blacklist position
int recfindneighbouring(int i, int j, int numToFindTab[], int pos, int size, int blacklist[]){
	//Check if it's the end
	if (pos >= size){
		return 1;
	}
	//Init counter
	int count = 0;
	// Search amongs neighbors
	for (int i2 = max(i - 1, 0); i2 <= min(i + 1, HEIGHT - 1); i2++){
		for (int j2 = max(j - 1, 0); j2 <= min(j + 1, WIDTH - 1); j2++){
			//If it's not blacklist and if the digit matches 
			if (isIn(i2, j2, blacklist, pos*2) == 0 && numToFindTab[pos] == matrix[i2][j2]){
				int blacklist2[size*2];
				//add the pos to the blacklist
				matriscopy(blacklist2, blacklist, size * 2);

                                blacklist2[pos*2] = i2;
                                blacklist2[pos*2 + 1] = j2;
				//Call the function again
				count += recfindneighbouring(i2, j2, numToFindTab, pos+1, size, blacklist2);
			}
		}
	}
	return count;
}

//Fill the matrix with random numbers
void fillMatrix(){
	for (int i = 0; i < HEIGHT; i++){
                for (int j = 0; j < WIDTH; j++){
			matrix[i][j] = rand() % 10;
		}
	}
}

//main function
void main()
{
	srand(time(NULL));
	unsigned char* num = itoa(numberToFind, 10);
	int size = strlen(num);
	int numToFindTab[size];
	for (int i = 0; i < size; i++){
		numToFindTab[i] = num[i] - '0';
	}
	fillMatrix();
	for (int i = 0; i < HEIGHT; i++){
		for (int j = 0; j < WIDTH; j++){
			printf("%d, ", matrix[i][j]);
		}
		printf("\n");
	}
		//split the number into a array
	printf("number to find : ");
        for (int i = 0; i < size; i++){
                printf("%d, ", numToFindTab[i]) ;
        }
	printf("\n");
	int counter = 0;


	double start;
	double end;
	
	//Start the clock
	start = omp_get_wtime();
	#pragma omp parallel for
	for (int i=0;i<HEIGHT;i++){
		for (int j=0;j<WIDTH;j++){
			if (matrix[i][j] == numToFindTab[0]){
				int blacklist[size*2];
				blacklist[0] = i;
				blacklist[1] = j;
				int find = recfindneighbouring(i, j, numToFindTab, 1, size, blacklist);
				//safe counter
				#pragma omp atomic
				counter += find;
			}
		}
	}
	//stop the clock
	end = omp_get_wtime();
	printf("Work took %f seconds\n", end - start);

	printf("find : %d\n", counter);
}
