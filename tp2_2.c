#include <stdio.h>
#include <mpi.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

// Size of the matrix
#define WIDTH 50
#define HEIGHT 50

//Number to Find

#define numberToFind 10


//Tags for MPI

#define send_matrix_tag 2001
#define send_start_tag 2002
#define send_stop_tag 2003
#define send_result_tag 2004



int matrix[HEIGHT * WIDTH];


// Transform a integer to a string
char* itoa(int val, int base){
	static char buf[32] = {0};
	int i = 30;
	for(; val && i ; --i, val /= base)
		buf[i] = "0123456789abcdef"[val % base];
	return &buf[i+1];
}


// Find the min between the two param 
int min(int a, int b){
	if (a > b)
		return b;
	return a;
}

// Find the max between the tow param
int max (int a, int b){
	if (a < b)
		return b;
	return a;
}

// Check if the position i,j is in the blacklist
int isIn(int i, int j, int blacklist[], int size){
	int pos = 0;
	while (pos < size){
		if (i == blacklist[pos] && j == blacklist[pos+1])
			return 1;
		else
			pos = pos + 2;
	}
	return 0;
}

//Copy the matrix
void matriscopy (void * destmat, void * srcmat, int size) 
{
  memcpy(destmat,srcmat, size*sizeof(int));
}


// This function searches recursively for the next number among the neighbors of the position set as argument. 
// Param : 
// i : the pos
// j : the pos
// numToFindTab : number to find
// pos : position of the digit to find
// size : size of the numberToFind
// blacklist : The blacklist position 
int recfindneighbouring(int i, int j, int numToFindTab[], int pos, int size, int blacklist[]){
	//Check if it's the end
	if (pos >= size){
		return 1;
	}
	//Init counter
	int count = 0;
	// Search amongs neighbors
	for (int i2 = max(i - 1, 0); i2 <= min(i + 1, HEIGHT - 1); i2++){
		for (int j2 = max(j - 1, 0); j2 <= min(j + 1, WIDTH - 1); j2++){
			//If it's not blacklist and if the digit matches 
			if (isIn(i2, j2, blacklist, pos*2) == 0 && numToFindTab[pos] == matrix[i2 * WIDTH + j2]){
				int blacklist2[size*2];
				//add the pos to the blacklist
				matriscopy(blacklist2, blacklist, size * 2);

                                blacklist2[pos*2] = i2;
                                blacklist2[pos*2 + 1] = j2;
								
				//Call the function again
				count += recfindneighbouring(i2, j2, numToFindTab, pos+1, size, blacklist2);
			}
		}
	}
	return count;
}

//Fill the matrix with random numbers
void fillMatrix(){
	for (int i = 0; i < HEIGHT; i++){
                for (int j = 0; j < WIDTH; j++){
			matrix[i * WIDTH + j] = rand() % 10;
		}
	}
}


//main function
void main(int argc, char **argv)
{
	MPI_Status status;
        int my_id, root_process, ierr, num_procs;

        ierr = MPI_Init(&argc, &argv);

        root_process = 0;
		//Find the id of the process and the number of process
        ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
        ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

		unsigned char* num = itoa(numberToFind, 10);
        int size = strlen(num);
        int numToFindTab[size];
		//split the number into a array
        for (int i = 0; i < size; i++){
                numToFindTab[i] = num[i] - '0';
        }
		
        if(my_id == root_process) {
			//main process
			srand(time(NULL));
			fillMatrix();
		
			//Print matrix
			for (int i = 0; i < HEIGHT; i++){
				for (int j = 0; j < WIDTH; j++){
					printf("%d, ", matrix[i * WIDTH + j]);
				}
				printf("\n");
			}
			sleep(1);
			
			//Print the number
			printf("number to find : ");
				for (int i = 0; i < size; i++){
						printf("%d, ", numToFindTab[i]) ;
				}
			printf("\n");
			
			//start of the clock
			double startT = MPI_Wtime();
			printf("sending the matrix...\n");
			for (int id = 1; id < num_procs; id++){
				//Send the matrix
				MPI_Send(&matrix, HEIGHT * WIDTH, MPI_INT, id, send_matrix_tag, MPI_COMM_WORLD);

				//Find the start and the stop of the process i
				int start = ((HEIGHT * WIDTH) * (id - 1)) / (num_procs - 1);
				int stop = ((HEIGHT * WIDTH) * (id)) / (num_procs - 1);
				//send
				MPI_Send(&start, 1, MPI_INT, id, send_start_tag, MPI_COMM_WORLD);
				MPI_Send(&stop, 1, MPI_INT, id, send_stop_tag, MPI_COMM_WORLD);
			}
			//Init counter
			int counter = 0;

			//For each slave, wait the result
			for (int id = 1; id < num_procs; id++){
				int result;
				ierr = MPI_Recv(&result, 1, MPI_INT, id, send_result_tag, MPI_COMM_WORLD, &status);
				counter += result;
			}
			
			//stop the clock
			double stopT = MPI_Wtime();

			//final print
			printf("Work took %f seconds\n", stopT - startT);

			printf("find : %d\n", counter);
		} else {
			
			//recv the matrix
			ierr = MPI_Recv(&matrix, HEIGHT * WIDTH, MPI_INT, root_process, send_matrix_tag, MPI_COMM_WORLD, &status);
			int start, stop;
			//recv the start and stop
			ierr = MPI_Recv(&start, 1, MPI_INT, root_process, send_start_tag, MPI_COMM_WORLD, &status);
			ierr = MPI_Recv(&stop, 1, MPI_INT, root_process, send_stop_tag, MPI_COMM_WORLD, &status);



			//init counter
			int counter = 0;

			int broke = 0;
			
			
			//Process of the slave
			for (int i=start/WIDTH;i<HEIGHT;i++){
				for (int j=start%WIDTH;j<WIDTH;j++){
							start = 0;
								  if (matrix[i * WIDTH + j] == numToFindTab[0]){
										  int blacklist[size*2];
										  blacklist[0] = i;
										  blacklist[1] = j;
										  counter += recfindneighbouring(i, j, numToFindTab, 1, size, blacklist);
								  }
					  if (i * WIDTH + j + 1 >= stop) {
						  //finish
						broke = 1;
						break;
					  }
				}
				if (broke == 1)
					break;
					}
					
					//send the result
				MPI_Send(&counter, 1, MPI_INT, root_process, send_result_tag, MPI_COMM_WORLD);
	}
 	ierr = MPI_Finalize();
}
